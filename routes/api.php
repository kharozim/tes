<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\InventoryController;
use App\Http\Controllers\API\InventoryItemController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\RackController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(
    [
        'prefix' => 'auth',
        'as' => 'auth.',
    ],
    function () {
        Route::post('/login',[AuthController::class, 'login'] );
        Route::post('/register',[AuthController::class, 'register'] );
        Route::post('/set-admin/{userId}',[AuthController::class, 'setAdmin'] );
        Route::post('/request-reset',[AuthController::class, 'requestReset'] )->name('forgot password');
        Route::post('/reset-password',[AuthController::class, 'reset'] )->name('reset password');
        
    }
);

Route::group(
    [
        'prefix' => 'product',
        'as' => 'product.',
        'middleware' => 'auth:sanctum'
    ],
    function () {
        Route::get('/all',[ProductController::class, 'all'] );
        Route::get('/scan/{code}',[ProductController::class, 'scan'] );
        Route::get('/detail/{productId}',[ProductController::class, 'detail'] );
        Route::post('/add',[ProductController::class, 'add'] );
        Route::put('/update/{productId}',[ProductController::class, 'update'] );
        Route::delete('/delete/{productId}',[ProductController::class, 'remove'] );
    }
);

Route::group(
    [
        'prefix' => 'rack',
        'as' => 'rack.',
        'middleware' => 'auth:sanctum'
    ],
    function () {
        Route::get('/all',[RackController::class, 'all'] );
        Route::get('/detail/{productId}',[RackController::class, 'detail'] );
        Route::post('/add',[RackController::class, 'add'] );
        Route::put('/update/{productId}',[RackController::class, 'update'] );
        Route::delete('/delete/{productId}',[RackController::class, 'remove'] );
    }
);

Route::group(
    [
        'prefix' => 'inventory',
        'as' => 'invetory.',
        'middleware' => 'auth:sanctum'
    ],
    function () {
        Route::get('/all',[InventoryController::class, 'all'] );
        Route::get('/detail/{inventoryId}',[InventoryController::class, 'detail'] );
        Route::post('/add',[InventoryController::class, 'add'] );
        Route::put('/update/{inventoryId}',[InventoryController::class, 'update'] );
        Route::delete('/delete/{inventoryId}',[InventoryController::class, 'remove'] );
    }
);

Route::group(
    [
        'prefix' => 'inventory-item',
        'as' => 'invetory-item.',
        'middleware' => 'auth:sanctum'
    ],
    function () {
        Route::get('/all/{inventoryId}',[InventoryItemController::class, 'all'] );
        Route::get('/detail/{inventoryId}',[InventoryItemController::class, 'detail'] );
        Route::post('/add',[InventoryItemController::class, 'add'] );
        Route::put('/update/{inventoryId}',[InventoryItemController::class, 'update'] );
        Route::delete('/delete/{inventoryId}',[InventoryItemController::class, 'remove'] );
    }
);



