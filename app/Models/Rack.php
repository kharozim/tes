<?php

namespace App\Models;

use App\Utils\FormatTimestamp;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rack extends Model
{
    use HasFactory,FormatTimestamp, SoftDeletes;

    protected $fillable = [
        'code',
        'capacity',
    ];


    protected $casts = [
        'capacity' => 'integer',
    ];
}
