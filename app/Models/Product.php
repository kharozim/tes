<?php

namespace App\Models;

use App\Utils\FormatTimestamp;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, FormatTimestamp, SoftDeletes;

    protected $fillable = [
        'name',
        'code',
        'category',
        'status',
    ];
}
