<?php

namespace App\Models;

use App\Utils\FormatTimestamp;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryItem extends Model
{
    use HasFactory, FormatTimestamp, SoftDeletes;


    protected $fillable = [
        'inventory_id',
        'product_id',
        'rack_id',
    ];


    protected $casts = [
        'inventory_id' => 'integer',
        'product_id' => 'integer',
        'rack_id' => 'integer',
    ];

    protected $appends = [
        'inventory',
        'product',
        'rack',
    ];

    public function inventory()
    {
        return $this->belongsTo(Inventory::class, 'inventory_id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
    public function rack()
    {
        return $this->belongsTo(Rack::class, 'rack_id');
    }



    public function getInventoryAttribute()
    {
        return $this->inventory()->select(['user_id', 'code'])->first();
    }

    public function getProductAttribute()
    {
        return $this->product()->select(['code', 'name', 'category'])->first();
    }

    public function getRackAttribute()
    {
        return $this->rack()->select(['code', 'capacity'])->first();
    }
}
