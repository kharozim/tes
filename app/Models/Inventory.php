<?php

namespace App\Models;

use App\Utils\FormatTimestamp;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model
{
    use HasFactory, FormatTimestamp, SoftDeletes;

    protected $fillable = [
        'code',
        'user_id',
        'finished_at',
    ];


    protected $casts = [
        'user_id' => 'integer',
    ];


    protected $appends = [
        'user',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getUserAttribute()
    {
        return $this->user()->select(['name', 'email'])->first();
    }
}
