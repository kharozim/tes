<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Inventory;
use App\Utils\ResponseUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InventoryController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        return $this->request = $request;
    }


    public function all()
    {
        $user = Auth::user();

        $result = Inventory::get();
        return ResponseUtil::success($result);
    }


    public function detail($inventoryId)
    {
        $user = Auth::user();

        $result = Inventory::find($inventoryId);

        if (!$result) {
            return ResponseUtil::error('Inventory tidak ditemukan', 403);
        }

        return ResponseUtil::success($result);
    }

    public function add()
    {

        $user = Auth::user();


        $this->validate($this->request, [
            'code' => 'required'
        ]);


        $request = $this->request->only([
            'code',
        ]);
        
        $request['user_id'] = $user->id;

        $result = Inventory::create($request);

        if (!$result) {
            return ResponseUtil::error('Gagal Menambah Inventory', 403);
        }

        return ResponseUtil::success($result->fresh());
    }

    public function update($inventoryId)
    {
        $user = Auth::user();

        $inventory = Inventory::find($inventoryId);
        if (!$inventory) {
            return ResponseUtil::error('Inventory tidak ditemukan', 400);
        }

        $request = $this->request->only([
            'code',
            'udpated_at',
        ]);

        $inventory->update($request);

        return ResponseUtil::success($inventory->fresh());
    }


    public function remove($inventoryId)
    {
        $user = Auth::user();

        $inventory = Inventory::find($inventoryId);
        if (!$inventory) {
            return ResponseUtil::error('Inventory tidak ditemukan', 400);
        }
        $inventory->delete();
        return ResponseUtil::success('Berhasil hapus Inventory');
    }
}
