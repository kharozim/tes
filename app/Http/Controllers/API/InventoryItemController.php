<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Inventory;
use App\Models\InventoryItem;
use App\Models\Product;
use App\Models\Rack;
use App\Utils\ResponseUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InventoryItemController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        return $this->request = $request;
    }


    public function all($inventoryId)
    {
        $user = Auth::user();

        $result = InventoryItem::where('inventory_id',$inventoryId)->get();

        return ResponseUtil::success($result);
    }


    public function detail($inventoryItemId)
    {
        $user = Auth::user();

        $result = InventoryItem::find($inventoryItemId);

        if (!$result) {
            return ResponseUtil::error('Inventory Item tidak ditemukan', 403);
        }

        return ResponseUtil::success($result);
    }

    public function add()
    {

        $user = Auth::user();

        $this->validate($this->request, [
            'inventory_id' => 'required | integer',
            'product_id' => 'required | integer',
            'rack_id' => 'required | integer'
        ]);

        $inventory = Inventory::find($this->request['inventory_id']);
        if(!$inventory){
            return ResponseUtil::error('Inventory tidak ditemukan', 403);
        }

        $product = Product::find($this->request['product_id']);
        if(!$product){
            return ResponseUtil::error('Product tidak ditemukan', 403);
        }

        $rack = Rack::find($this->request['rack_id']);
        if(!$rack){
            return ResponseUtil::error('Rack tidak ditemukan', 403);
        }


        $request = $this->request->only([
            'inventory_id',
            'product_id',
            'rack_id'
        ]);

        $result = InventoryItem::create($request);

        if (!$result) {
            return ResponseUtil::error('Gagal Menambah Inventory Item', 403);
        }

        return ResponseUtil::success($result->fresh());
    }

    public function update($inventoryItemId)
    {
        $user = Auth::user();
      
        $inventory = InventoryItem::find($inventoryItemId);
        if (!$inventory) {
            return ResponseUtil::error('Inventory Item tidak ditemukan', 400);
        }

        $this->validate($this->request, [
            'inventory_id' => 'integer',
            'product_id' => 'integer',
            'rack_id' => 'integer'
        ]);

        $request = $this->request->only([
            'inventory_id',
            'product_id',
            'rack_id',
        ]);

        $inventory->update($request);

        return ResponseUtil::success($inventory->fresh());
    }


    public function remove($inventoryItemId)
    {
        $user = Auth::user();

        $inventory = InventoryItem::find($inventoryItemId);
        if (!$inventory) {
            return ResponseUtil::error('Inventory Item tidak ditemukan', 400);
        }
        $inventory->delete();
        return ResponseUtil::success('Berhasil hapus Inventory Item');
    }
}
