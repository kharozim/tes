<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ResetPassword;
use App\Models\User;
use App\Utils\HelperUtil;
use App\Utils\ResponseUtil;
use Carbon\Carbon;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    public function register()
    {
        $this->request->validate([
            'name' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required'],
            'nip' => ['required', 'integer'],
        ]);

        $request = $this->request->only(['name', 'email',  'password', 'image', 'nip']);
        $user = User::where('email', 'like', '%' . trim($request['email']) . '%')->first();
        if ($user) {
            return ResponseUtil::error('User sudah ditambahkan', 400);
        }


        $request['role_id'] = 1;
        $request['password'] = bcrypt($request['password']);

        $userResponse = User::create($request);

        $token = $userResponse->createToken('auth_token')->plainTextToken;
        $data = [
            'id' => $userResponse->id,
            'name' => $userResponse->name,
            'email' => $userResponse->email,
            'role_id' => $userResponse->role_id,
            'nip'=>$userResponse->nip,
            'access_token' => $token
        ];

        return ResponseUtil::success($data);
    }

    public function login()

    {
        $this->request->validate([
            'nip' => ['required', 'integer'],
            'password' => 'required'
        ]);

        $request = $this->request->only(['nip', 'password']);

        $user = User::where('nip', '=', trim($request['nip']))->first();
        if (!$user) {
            return ResponseUtil::error('NIP tidak ditemukan', 400);
        }


        if (Auth::attempt($request, false)) {
            $token = $user->createToken('auth_token')->plainTextToken;
            $data = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'nip' => $user->nip,
                'role_id' => $user->role_id,
                'access_token' => $token
            ];
            return ResponseUtil::success($data);
        }

        return ResponseUtil::error('Password Salah', 400);
    }

    public function requestReset()

    {
        $this->request->validate([
            'email' => ['required', 'email'],
        ]);

        $user = USer::where('email', 'like', '%' . trim($this->request->email) . '%')->first();

        if(!$user){
            return ResponseUtil::error('Email tidak ditemukan', 400);
        }
        $token  = HelperUtil::randomChar(32);

        $payloads   = [
            'token' => $token,
            'user_id' => $user->id,
        ];

        $passwordReset  = ResetPassword::create($payloads);
        $expiredAt  = Carbon::now()->addHours(3)->format("Y-m-d H:i:s");

        $result = [  'token_reset_password' => $passwordReset->token,'email' => $this->request->email, 'expired_at' => $expiredAt];

        return ResponseUtil::success($result);


    }

    public function reset()
    {


        $this->validate($this->request, [
            'password' => ['required', 'min:8'],
            'token' => ['required'],
        ]);

        $token  = $this->request->token;
        $passwordReset  = ResetPassword::where('token', '=', $token)->first();
        if( !$passwordReset ) {
            return ResponseUtil::error('Not found', 404);
        }



        $user   = User::find($passwordReset->user_id);

        $user->update([
            'password' => bcrypt($this->request->password),
        ]);

        ResetPassword::where('user_id', '=', $user->id)->delete();

        return ResponseUtil::success('Berhasil reset password');

    }


    public function setAdmin($userId)
    {

        $user = User::find($userId);

        if (!$user) {
            return ResponseUtil::error('User tidak ditemukan', 400);
        }
        $user->role_id = 2;
        $user->save();
        return ResponseUtil::success($user);
    }
}
