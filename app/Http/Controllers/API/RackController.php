<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Rack;
use App\Utils\ResponseUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RackController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        return $this->request = $request;
    }


    public function all()
    {
        $user = Auth::user();


        $result = Rack::get();
        return ResponseUtil::success($result);
    }


    public function detail($RackId)
    {
        $user = Auth::user();

        $result = Rack::find($RackId);

        if (!$result) {
            return ResponseUtil::error('Rak tidak ditemukan', 403);
        }

        return ResponseUtil::success($result);
    }

    public function add()
    {

        $user = Auth::user();
        if ($user->role_id != 2) {
            return ResponseUtil::error('Anda bukan admin', 400);
        }

        $this->validate($this->request, [
            'code' => 'required',
            'capacity' => 'required | integer',
        ]);


        $request = $this->request->only([
            'code',
            'capacity',
        ]);

        $result = Rack::create($request);

        if (!$result) {
            return ResponseUtil::error('Gagal Menambah Rak', 403);
        }

        return ResponseUtil::success($result->fresh());
    }

    public function update($RackId)
    {
        $user = Auth::user();
        if ($user->role_id != 2) {
            return ResponseUtil::error('Anda bukan admin', 400);
        }

        $Rak = Rack::find($RackId);
        if (!$Rak) {
            return ResponseUtil::error('Rak tidak ditemukan', 400);
        }

        $this->validate($this->request, [
            'capacity' => 'integer',
        ]);

        $request = $this->request->only([
            'code',
            'capacity',
        ]);

        $Rak->update($request);

        return ResponseUtil::success($Rak->fresh());
    }


    public function remove($RackId)
    {
        $user = Auth::user();
        if ($user->role_id != 2) {
            return ResponseUtil::error('Anda bukan admin', 400);
        }
        $Rak = Rack::find($RackId);
        if (!$Rak) {
            return ResponseUtil::error('Rak tidak ditemukan', 400);
        }
        $Rak->delete();
        return ResponseUtil::success('Berhasil hapus Rak');
    }
}
