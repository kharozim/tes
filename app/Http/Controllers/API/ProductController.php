<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Utils\ResponseUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{


    protected $request;

    public function __construct(Request $request)
    {
        return $this->request = $request;
    }


    public function all()
    {
        $user = Auth::user();

        $result = Product::get();
        return ResponseUtil::success($result);
    }


    public function detail($productId)
    {
        $user = Auth::user();


        $result = Product::find($productId);

        if (!$result) {
            return ResponseUtil::error('Produk tidak ditemukan', 403);
        }

        return ResponseUtil::success($result);
    }

    public function scan($code)
    {
        $user = Auth::user();


        $result = Product::where('code',$code)->first();

        if (!$result) {
            return ResponseUtil::error('Produk tidak ditemukan', 403);
        }

        return ResponseUtil::success($result);
    }

    public function add()
    {

        $user = Auth::user();
        if ($user->role_id != 2) {
            return ResponseUtil::error('Anda bukan admin', 400);
        }

        $this->validate($this->request, [
            'name' => 'required',
            'category' => 'required',
            'status' => 'required',
        ]);


        $request = $this->request->only([
            'name',
            'category',
            'status',
        ]);

        $result = Product::create($request);

        if (!$result) {
            return ResponseUtil::error('Gagal Menambah Produk', 403);
        }

        $result->update(['code' => 'PRODUK' . $result['id']]);

        return ResponseUtil::success($result->fresh());
    }

    public function update($productId)
    {
        $user = Auth::user();
        if ($user->role_id != 2) {
            return ResponseUtil::error('Anda bukan admin', 400);
        }

        $produk = Product::find($productId);
        if (!$produk) {
            return ResponseUtil::error('Produk tidak ditemukan', 400);
        }

        $this->validate($this->request, [
            'price' => 'integer',
        ]);

        $request = $this->request->only([
            'name',
            'code',
            'category',
            'status',
        ]);

        $produk->update($request);

        return ResponseUtil::success($produk->fresh());
    }


    public function remove($productId)
    {
        $user = Auth::user();
        if ($user->role_id != 2) {
            return ResponseUtil::error('Anda bukan admin', 400);
        }
        $produk = Product::find($productId);
        if (!$produk) {
            return ResponseUtil::error('Produk tidak ditemukan', 400);
        }
        $produk->delete();
        return ResponseUtil::success('Berhasil hapus Produk');
    }
}
